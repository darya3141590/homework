import math
import sys
import random
import pygame
import pygame_menu
import os.path as path
import tkinter
from tkinter import messagebox
pygame.font.init()

messagebox.showinfo('Снаке', 'Produced by $Husha')
pygame.init()
pygame.mixer.music.load("sounds/Menu.mp3")
pygame.mixer.music.play(loops=0, start=0.0, fade_ms = 0)
bg=pygame.image.load("resources/fon.png")
width=700
bg = pygame.transform.scale(bg, (width, width))
rect = bg.get_rect()
main_font=pygame.font.SysFont("Verdana", 18)
class cube(object):
    rows = 50
    w = 700
    def __init__(self, start, dirnx=1, dirny=0, color=(255, 0, 0)):
        self.pos = start
        self.dirnx = 1
        self.dirny = 0
        self.color = color
        if self.color==(0,100,0):
            self.image = pygame.image.load("resources/apple.jpeg").convert()

    def move(self, dirnx, dirny):
        self.dirnx = dirnx
        self.dirny = dirny
        self.pos = (self.pos[0] + self.dirnx, self.pos[1] + self.dirny)

    def draw(self, surface, eyes=False):
        # self.x = 14
        # self.y = 14
        # apple_rect = self.image.get_rect(
        #     topleft=(self.x, self.y))
        # win.blit(self.image, apple_rect)
        dis = self.w // self.rows
        i = self.pos[0]
        j = self.pos[1]
        pygame.draw.rect(surface, self.color, (i * dis + 1, j * dis + 1, dis - 2, dis - 2))
        # win.blit(self.image, (self.x, self.y))
        if eyes:
            centre = dis // 2
            radius = 2
            circleMiddle = (i * dis + centre - radius, j * dis + 8)
            circleMiddle2 = (i * dis + dis - radius * 2, j * dis + 8)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle, radius)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle2, radius)


class snake(object):
    body = []
    turns = {}
    def __init__(self, color, pos):
        self.color = color
        self.head = cube(pos) #только инициализация головы
        self.body.append(self.head)
        self.dirnx = 0
        self.dirny = 1

    def move(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            keys = pygame.key.get_pressed()
            for key in keys:
                if keys[pygame.K_LEFT] or keys[ord('a')]:
                    self.dirnx = -1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_RIGHT] or keys[ord('d')]:
                    self.dirnx = 1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_UP] or keys[ord('w')]:
                    self.dirnx = 0
                    self.dirny = -1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_DOWN] or keys[ord('s')]:
                    self.dirnx = 0
                    self.dirny = 1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_ESCAPE]:
                    menuu()

        for i, c in enumerate(self.body):
            p = c.pos[:]
            if p in self.turns:
                turn = self.turns[p]
                c.move(turn[0], turn[1])
                if i == len(self.body) - 1:
                    self.turns.pop(p)
            else:
                if (c.dirnx == -1 and c.pos[0] <= 0) or (c.dirnx == 1 and c.pos[0] >= c.rows - 1) or (c.dirny == 1 and c.pos[1] >= c.rows - 1) or (c.dirny == -1 and c.pos[1] <= 0):
                    print('Score: ', len(s.body)-1)
                    pygame.mixer.music.pause()
                    sound1 = pygame.mixer.Sound('sounds/bumps.mp3')
                    sound1.play()
                    message_box('You Lost!', str(user.get_value()) + ", "+"your score: "+str(len(s.body)-1))
                    s.reset((25, 25))
                    break
                else:
                    c.move(c.dirnx, c.dirny)

    def reset(self, pos):
        pygame.mixer.music.unpause()
        self.head = cube(pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 1

    def addCube(self):
        tail = self.body[-1]
        dx, dy = tail.dirnx, tail.dirny

        if dx == 1 and dy == 0:
            self.body.append(cube((tail.pos[0] - 1, tail.pos[1])))
        elif dx == -1 and dy == 0:
            self.body.append(cube((tail.pos[0] + 1, tail.pos[1])))
        elif dx == 0 and dy == 1:
            self.body.append(cube((tail.pos[0], tail.pos[1] - 1)))
        elif dx == 0 and dy == -1:
            self.body.append(cube((tail.pos[0], tail.pos[1] + 1)))

        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for i, c in enumerate(self.body):
            if i == 0:
                c.draw(surface, True)
            else:
                c.draw(surface)



def drawGrid(w, rows, surface):
    sizeBtwn = w // rows
    x = 0
    y = 0
    for l in range(rows):
        x = x + sizeBtwn
        y = y + sizeBtwn
        pygame.draw.line(surface, (255, 255, 255), (x, 0), (x, w))
        pygame.draw.line(surface, (255, 255, 255), (0, y), (w, y))


def redrawWindow(surface):
    global rows, width, s, snack
    # surface.fill((0, 0, 0))
    s.draw(surface)
    snack.draw(surface)
    # drawGrid(width, rows, surface)
    pygame.display.update()


def randomSnack(rows, item):
    positions = item.body
    if len(positions)>1:
        pygame.mixer.music.pause()
        sound1 = pygame.mixer.Sound('sounds/nyam.mp3')
        sound1.play(maxtime=300)
        pygame.mixer.music.unpause()
    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)
        if len(list(filter(lambda z: z.pos == (x, y), positions))) > 0:
            continue
        else:
            break

    return (x, y)


def message_box(subject, content):
    messagebox.showinfo(subject, content)


def menuu():
    global mytheme, myimage, font, menu, i
    width = 512
    height = 340
    win = pygame.display.set_mode((width, height))
    mytheme = pygame_menu.themes.THEME_SOLARIZED.copy()
    myimage = pygame_menu.baseimage.BaseImage(
        image_path="466.jpg",
        drawing_mode=pygame_menu.baseimage.IMAGE_MODE_REPEAT_XY,
    )
    mytheme.background_color = myimage
    font = pygame_menu.font.FONT_8BIT
    mytheme.widget_font=font
    mytheme.title_font = font
    mytheme.title_font_color=(125,229,19)
    mytheme.title_background_color = (255, 33, 18)

    menu = pygame_menu.Menu('Welcome', 512, 340,
                            theme=mytheme)
    menu.add.vertical_margin(100)
    if i==1:
        menu.add.button('Play', name, font_color=(255,68,68), selection_color=(255,255,255))
    else:
        i=1
        menu.add.button('Back to game', main, font_color=(255,68,68), selection_color=(255,255,255))
        menu.add.button('Play', name, font_color=(255,68,68), selection_color=(255,255,255))
    menu.add.button('About', about, font_color=(255,68,68), selection_color=(255,255,255))
    menu.add.button('Quit', pygame_menu.events.EXIT, font_color=(255,68,68), selection_color=(255,255,255))
    events = pygame.event.get()
    menu.mainloop(win)

    return mytheme, menu
def about():
    global flag
    if flag:
        flag = False
        INSTR = ['Press ESC to enable/disable Menu',
                'Press UP/DOWN or W/S to move to the up and down respectively',
                'Press LEFT/RIGHT or A/D to move to the left and right respectively']
        help_menu.clear()
        help_menu.set_title("Instructions")
        for m in INSTR:
            help_menu.add.label(m, align=pygame_menu.locals.ALIGN_CENTER)
        help_menu.add.vertical_margin(25)
        help_menu.add.button('Return to Menu', menuu, selection_color=(255,68,68))
        help_menu.mainloop(win)
    else:
        help_menu.mainloop(win)

def name():
    global i, user
    i=3
    menu.clear()
    font_sec=pygame.font.SysFont("FONT_8BIT", 10)
    menu.add.vertical_margin(80)
    user=menu.add.text_input('Your name is ', default='Kulebyaka', font_size=22, font_color=(255,68,68), selection_color=(255,255,255))
    menu.add.vertical_margin(30)
    menu.add.button('Continue', main, font_size=19, font_color=(255,68,68), selection_color=(255,255,255))
    menu.mainloop(win)

def score():
    global win, s
    main_score=main_font.render("Score: " + str(len(s.body)-1), True, (255,255,255))
    win.blit(main_score, [325,0])
    
def main():
    global width, rows, s, snack, i, font
    width = 700
    rows = 50
    win = pygame.display.set_mode((width, width))
    if i==1:
        i=2
    if s!=0:
        if i==3:
            pygame.mixer.music.load("sounds/Menu.mp3")
            pygame.mixer.music.play(loops=0, start=0.0, fade_ms=0)
            s.reset((25, 25))
            snack = cube(randomSnack(rows, s), color=(68, 191, 38))
        else:
            pass
    else:
        s = snake((255, 0, 0), (25, 25)) #инициализировали первый куб(голову)
        snack = cube(randomSnack(rows, s), color=(68, 191, 38))
    flag = True

    clock = pygame.time.Clock()
    while flag:
        score()
        pygame.time.delay(50)
        clock.tick(10)
        s.move()
        if s.body[0].pos == snack.pos:
            # text = font.render("World Мир", False,(0, 180, 0))
            #
            # win.blit(text, (10, 50))
            # pygame.display.update()

            s.addCube()
            snack = cube(randomSnack(rows, s), color=(68, 191, 38))

        for x in range(len(s.body)):
            if s.body[x].pos in list(map(lambda z: z.pos, s.body[x + 1:])):
                print('Score: ', len(s.body)-1)
                pygame.mixer.music.pause()
                sound1 = pygame.mixer.Sound('sounds/bumps.mp3')
                sound1.play()
                message_box('You Lost!', str(user.get_value()) + ", "+"your score: "+str(len(s.body)-1))
                s.reset((25, 25))
                break

        redrawWindow(win)
        win.blit(bg, rect)
    return i
i=1
s=0
width = 512
height=340
rows = 50
# bg=pygame.image.load("resources/fon.png")
# bg = pygame.transform.scale(bg, (width, width))
# rect = bg.get_rect()
flag=True
win = pygame.display.set_mode((width, height))
pygame.display.set_caption('SnAke')
pygame.display.update()
HELP = ['Press ESC to enable/disable Menu',
            'Press ENTER to access a Sub-Menu or use an option',
            'Press UP/DOWN to move through Menu']
help_theme = pygame_menu.Theme(
    background_color=(113, 226, 22, 230),  # 75% opacity
    title_background_color=(254, 41, 27, 230),
    title_font=pygame_menu.font.FONT_FRANCHISE,
    title_font_size=40,
    widget_font=pygame_menu.font.FONT_FRANCHISE,
    widget_font_color=(255,255,255),
    widget_font_shadow=True,
    widget_font_shadow_position=pygame_menu.locals.POSITION_SOUTHEAST,
    widget_font_size=25
)
help_menu = pygame_menu.Menu(
    height=340,  # Fullscreen
    theme=help_theme,
    title='Help',
    width=512
)
for m in HELP:
    help_menu.add.label(m, align=pygame_menu.locals.ALIGN_CENTER)
help_menu.add.vertical_margin(25)
help_menu.add.button('Go to Menu', menuu, selection_color=(255,68,68))
help_menu.mainloop(win)