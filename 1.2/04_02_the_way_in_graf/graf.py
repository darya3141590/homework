import pandas as pd
import matplotlib.pyplot as plt
import tkinter as tk
#from networkx import
INF = 999999


class Graph:

    def __init__(self):
        self.adj_matrix = pd.DataFrame()
        # print(self.adj_matrix)

    def read_from_file(self, path):
        self.adj_matrix = pd.read_csv(path, sep=' ', header=0, index_col=0)
        # print(self.adj_matrix)
    #минимальный путь

    def get_minimal_road(self, from_node, to_node):
        weights = self._dijkstra_algorithm(from_node)
        road = [to_node]
        weight = weights[to_node]
        while from_node != to_node:
            for node in weights.keys():
                if self.adj_matrix[node][to_node] != 0:
                    current_weight = weight - self.adj_matrix[node][to_node]
                    if current_weight == weights[node]:
                        weight = current_weight
                        to_node = node
                        road.append(node)
        return road[::-1]

    #метод который вернет максимально близкую точку к вызванной, либо если можно добраться до нее вернет ее
    def get_node_closer(self, from_node, to_node):
        if self.get_minimal_weight(from_node, to_node) == INF:
            min_neighbour_node = from_node
            for num in self.adj_matrix.keys()[1:]:
                if 0 < self.adj_matrix[to_node][num] < self.adj_matrix[to_node]['A']:
                    min_neighbour_node = num

            assert min_neighbour_node == INF

            return min_neighbour_node
        else:
            return to_node

    #метод вернет минимальную длину пути
    def get_minimal_weight(self, from_node, to_node):
        return self._dijkstra_algorithm(from_node)[to_node]

    #алгоритм дийсктра который возвращает все пути от заданной точки
    def _dijkstra_algorithm(self, start_node):
        weights = {num: INF for num in self.adj_matrix.keys()}
        # print(weights)
        visited_nodes = {num: False for num in self.adj_matrix.keys()}
        weights[start_node] = 0
        for node in visited_nodes:
            min_weight = INF
            node_with_min_weight = None
            for node_neighbour in self.adj_matrix[node].keys():
                weight_neighbour = weights[node_neighbour]
                if not visited_nodes[node_neighbour] and weight_neighbour < min_weight:
                    min_weight = weight_neighbour
                    node_with_min_weight = node_neighbour

            if node_with_min_weight:
                for to_node in self.adj_matrix[node_with_min_weight].keys():
                    to_weight = self.adj_matrix[node_with_min_weight][to_node]
                    if to_weight > 0:
                        current_weight = min_weight + to_weight
                        if current_weight < weights[to_node]:
                            weights[to_node] = current_weight
                visited_nodes[node_with_min_weight] = True
        return weights



baseWindow=tk.Tk()
baseWindow.title("Нахождение пути в графе")
baseWindow.geometry("640x480")

def on_click_button():
    global graph
    global from_node
    global to_node
    global length
    global length_value
    global way
    global way_value

    desired_value = str(answer.get()) #путь
    from_node = desired_value[0]
    to_node = desired_value[1]
    graph = Graph()
    graph.read_from_file('msm.txt')

    if r_var.get()==1:
        # graph.get_minimal_weight(from_node, to_node)
        # graph.get_minimal_road(from_node, to_node)
        length.configure(text="Короткий путь равен = ")
        length_value.configure(text=graph.get_minimal_weight(from_node, to_node))
        way.configure(text="Путь равен = ")
        way_value.configure(text=graph.get_minimal_road(from_node, to_node))












question=tk.Label(baseWindow, text="Что найти?", fg="#FF4678", font="Arial 18")
question.pack()
answer=tk.Entry(baseWindow, fg="#FF1678", font="Arial 18", bg="lightblue", borderwidth="5", width="35", justify="center")
answer.pack()
second_question=tk.Label(baseWindow, text="Выберите: ", fg="#FF4678", font="Arial 18")
second_question.pack()
r_var = tk.IntVar()
ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Короткий путь", font="Arial 18",
                                      variable=r_var, justify="center", value=1)
ans1.pack()
button=tk.Button(baseWindow, command=on_click_button, text="Продолжить", fg="#FF1678", font="Arial 18", bg="lightblue", borderwidth="5", width="35", justify="center")
button.pack()
length = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
length.pack()
length_value = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
length_value.pack()
way = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
way.pack()
way_value = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
way_value.pack()

baseWindow.mainloop()


