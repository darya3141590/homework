import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx

INF = 999999


class Graph:

    def __init__(self):
        self.adj_matrix = pd.DataFrame()

    def read_from_file(self, path):
        self.adj_matrix = pd.read_csv(path, sep=' ', header=0, index_col=0)
        # self.adj_matrix = pd.read_csv(path, sep=' ')
        # print(self.adj_matrix)

    #минимальный путь

    def get_minimal_road(self, from_node, to_node):
        weights = self._dijkstra_algorithm(from_node)
        print(from_node)
        road = [to_node]
        weight = weights[to_node]
        print(self.adj_matrix)
        while from_node != to_node:
            for node in weights.keys():
                if self.adj_matrix[node][to_node] != 0:
                    current_weight = weight - self.adj_matrix[node][to_node]
                    if current_weight == weights[node]:
                        weight = current_weight
                        to_node = node
                        road.append(node)
        return road[::1]

    #метод который вернет максимально близкую точку к вызванной, либо если можно добраться до нее вернет ее
    def get_node_closer(self, from_node, to_node):
        if self.get_minimal_weight(from_node, to_node) == INF:
            min_neighbour_node = 'A'
            for num in self.adj_matrix.keys()[1:]:
                if 0 < self.adj_matrix[to_node][num] < self.adj_matrix[to_node][min_neighbour_node]:
                    min_neighbour_node = num

            if self.adj_matrix[from_node][min_neighbour_node] == 0:
                print("Пути нет(")
            from_node=min_neighbour_node

            return from_node
        else:
            return to_node

    #метод вернет минимальную длину пути
    def get_minimal_weight(self, from_node, to_node):
        # return INF
        return self._dijkstra_algorithm(from_node)[to_node]

    #алгоритм дийсктра который возвращает все пути от заданной точки
    def _dijkstra_algorithm(self, start_node):
        weights = {num: INF for num in self.adj_matrix.keys()} #минимальные пути до каждой из вершин
        visited_nodes = {num: False for num in self.adj_matrix.keys()} #запоминаем посещенные вершины
        weights[start_node] = 0
        for node in visited_nodes: #ищем узлы с минимальным путем
            min_weight = INF
            node_with_min_weight = None
            for node_neighbour in self.adj_matrix[node].keys():
                weight_neighbour = weights[node_neighbour]
                if not visited_nodes[node_neighbour] and weight_neighbour < min_weight:
                    min_weight = weight_neighbour
                    node_with_min_weight = node_neighbour

            if node_with_min_weight: #ищем от минимального узла различные пути до вершин
                for to_node in self.adj_matrix[node_with_min_weight].keys():
                    to_weight = self.adj_matrix[node_with_min_weight][to_node]
                    # to_weight = self.adj_matrix[to_node][node_with_min_weight]
                    if to_weight > 0:
                        current_weight = min_weight + to_weight
                        if current_weight < weights[to_node]: #и если эти новые пути меньше, чем те, которые напрямую, добавляем
                            weights[to_node] = current_weight
                visited_nodes[node_with_min_weight] = True
        return weights

    def graph_visualisation(self, road):
        g = nx.DiGraph(nx.from_pandas_adjacency(self.adj_matrix))
        road = [(road[i], road[i+1]) for i in range(len(road)-1)]
        pos = nx.spring_layout(g) #размечаем узлы
        labels = nx.get_edge_attributes(g, 'weight')
        labelss = [(labels[l]+0.5)/10 for l in labels]

        nx.draw_networkx_edges(g, pos, alpha=0.2, width=labelss) #распологаем на нужном расстонии ветки
        nx.draw_networkx_edges(g, pos, alpha=1, width=2, edgelist=road, edge_color='red')
        nx.draw_networkx_nodes(g, pos, edgecolors='lightgray') #место под узлы
        nx.draw_networkx_labels(g, pos, font_color='white') #узлы
        nx.draw_networkx_edge_labels(g, pos, edge_labels=labels)
        return plt.gcf() #выводим текущий граф




graph = Graph()
graph.read_from_file('msm.txt')
#print(graph.get_node_closer('A','B'))
# print(graph.get_minimal_weight('F', 'A'))
# print(graph.get_minimal_road('F', 'A'))

#graph.graph_visualisation()
