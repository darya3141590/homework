import tkinter as tk
from Graph import Graph
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
baseWindow=tk.Tk()
baseWindow.title("Нахождение пути в графе")
baseWindow.geometry("800x1000")

def on_click_button():
    global graph
    global from_node
    global to_node
    global length
    global length_value
    global way
    global way_value
    global canvas
    global figure
    global INF
    desired_value = str(answer.get().strip().replace(' ', '')) #путь
    #assert len(desired_value) != 2

    from_node = desired_value[1].strip().upper()
    to_node = desired_value[0].strip().upper()
    graph = Graph()
    graph.read_from_file('msm.txt')

    if r_var.get()==1:
        # graph.get_minimal_weight(from_node, to_node)
        # graph.get_minimal_road(from_node, to_node)
        figure.clf() #очищаем место для будущего графа
        if graph.get_minimal_weight(from_node, to_node) == INF:
            nearest_point.configure(text="Путь не существует, ближайшая точка равна = ")
            nearest_point_value.configure(text=graph.get_node_closer(from_node, to_node))
            from_node=graph.get_node_closer(from_node, to_node)
            length.configure(text="Короткий путь равен = ")
            length_value.configure(text=graph.get_minimal_weight(from_node, to_node))
            way.configure(text="Путь равен = ")
            way_value.configure(text="->".join(graph.get_minimal_road(from_node, to_node)))
        else:
            length.configure(text="Короткий путь равен = ")
            length_value.configure(text=graph.get_minimal_weight(from_node, to_node))
            way.configure(text="Путь равен = ")
            way_value.configure(text="->".join(graph.get_minimal_road(from_node, to_node)))
        graph.graph_visualisation(graph.get_minimal_road(from_node, to_node))
        canvas.draw()










INF = 999999
question=tk.Label(baseWindow, text="Что найти?", fg="#FF4678", font="Arial 18")
question.pack()
answer = tk.Entry(baseWindow, fg="#FF1678", font="Arial 18", bg="lightblue", borderwidth="5", width="35", justify="center")
answer.pack()
second_question = tk.Label(baseWindow, text="Выберите: ", fg="#FF4678", font="Arial 18")
second_question.pack()
r_var = tk.IntVar()
ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Короткий путь", font="Arial 18",
                                      variable=r_var, justify="center", value=1)
ans1.pack()
button=tk.Button(baseWindow, command=on_click_button, text="Продолжить", fg="#FF1678", font="Arial 18", bg="lightblue", borderwidth="5", width="35", justify="center")
button.pack()
nearest_point = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
nearest_point.pack()
nearest_point_value = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
nearest_point_value.pack()
length = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
length.pack()
length_value = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
length_value.pack()
way = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
way.pack()
way_value = tk.Label(baseWindow, text="", fg="#FF4678", font="Arial 18")
way_value.pack()

figure = plt.figure(figsize=(5, 4)) #создаем белое окно для графика(область)
canvas = FigureCanvasTkAgg(figure, master=baseWindow)
canvas.get_tk_widget().config(width='800', height='800')
canvas.get_tk_widget().pack()

baseWindow.mainloop()
