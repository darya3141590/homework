class DOM:
    def __init__(self, text=None, subelement=None):
        self.text = text
        self.subelement = subelement

    def __str__(self):
        content = "<{}>\n".format(self.__class__.__name__)
        if self.text:
            content += "{}\n".format(self.text)
        if self.subelement:
            content += str(self.subelement)

        content += "</{}>\n".format(self.__class__.__name__)
        return content


class Html(DOM):
    def __init__(self, text=None, subelement=None):
        super().__init__(text, subelement)
    def _str_(self):
        return super().__str__()

class Body(DOM):
    def __init__(self, text=None, subelement=None):
        super().__init__(text, subelement)

    def __str__(self):
        return super().__str__()

class P(DOM):
    def __init__(self, text=None, subelement=None):
        super().__init__(text, subelement)
    def __str__(self):
        return super().__str__()

para = P(text="this is some body text")
doc_body = Body(text="This is the body", subelement = para)
doc = Html(subelement = doc_body)
print(doc)
