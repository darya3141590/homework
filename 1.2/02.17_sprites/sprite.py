import pygame
import random
import tkinter
from tkinter import messagebox

messagebox.showinfo("Shlepa's adventure", "Preparing...")

from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
    RLEACCEL
)


SCREEN_WIDTH = 827
SCREEN_HEIGHT = 465

PURPLE = (255, 204, 255)
BLACK = (0, 0, 0)

pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
background_position=[0,0]
background_image=pygame.image.load("fon.jpeg").convert()
clock = pygame.time.Clock()

enemySprite = pygame.sprite.Sprite()
enemySpeed = 0

def newmob():
    m = Enemy()
    all_sprites.add(m)
    mobs.add(m)
    # for i in range(4):
    #     m = Enemy()
    #     all_sprites.add(m)
    #     mobs.add(m)

def draw_lives(surf, x, y, lives, img):
    for i in range(lives):
        img_rect = img.get_rect()
        img_rect.x = x + 30 * i
        img_rect.y = y
        surf.blit(img, img_rect)

class Player(pygame.sprite.Sprite):
    # global x
    # global y
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("shlepa.jpeg").convert()
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.centerx = SCREEN_WIDTH / 2
        self.rect.bottom = SCREEN_HEIGHT - 10
        self.speedx = 0

        self.lives = 3
        self.hidden = False
        self.hide_timer = pygame.time.get_ticks()

    def update(self, pressed_keys):
        if self.hidden and pygame.time.get_ticks() - self.hide_timer > 1000:
            self.hidden = False
            self.rect.centerx = SCREEN_WIDTH / 2
            self.rect.bottom = SCREEN_HEIGHT - 10
        self.speedx = 0
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -5)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 5)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-5, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(5, 0)

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
        # x = self.rect.left
        # y = self.rect.top

    def shoot(self):
        bullet = Bullet(self.rect.centerx, self.rect.top, self.rect.bottom)
        all_sprites.add(bullet)
        bullets.add(bullet)

    def hide(self):
        self.hidden = True
        self.hide_timer = pygame.time.get_ticks()
        self.rect.center = (SCREEN_WIDTH / 2, SCREEN_HEIGHT + 200)


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("raketa.png").convert()
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.image.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speedy = random.randint(5, 10)

    def update(self, pressed_keys):
        self.rect.move_ip(-self.speedy, 0)
        if self.rect.right < 0:
            self.rect = self.image.get_rect(
                center=(
                    random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                    random.randint(0, SCREEN_HEIGHT),
                )
            )

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, z):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("fire.png").convert()
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.bottom = (y+z)/2
        self.rect.centerx = x
        self.speedy = 10

    def update(self, pressed_keys):
        self.rect.x += self.speedy
        if self.rect.bottom < 0:
            self.kill()

player_img=pygame.sprite.Sprite
player_img.image = pygame.image.load("shlepa.jpeg").convert()
player_mini_img = pygame.transform.scale(player_img.image, (25, 19))
player_mini_img.set_colorkey(BLACK)
all_sprites = pygame.sprite.Group()


player = Player()
all_sprites.add(player)
mobs = pygame.sprite.Group()
for i in range (8):
    newmob()
# newmob()


bullets = pygame.sprite.Group()
slain = False
running = True

while running:
    for event in pygame.event.get():
        if event.type == K_ESCAPE:
            running = False
        elif event.type == KEYDOWN:
            if event.key == pygame.K_SPACE:
                player.shoot()
            if event.key == pygame.K_q:
                running = False
        elif event.type == pygame.QUIT:
            running = False

    pressed_keys = pygame.key.get_pressed()

    all_sprites.update(pressed_keys)

    hits = pygame.sprite.groupcollide(mobs, bullets, True, True)
    for hit in hits:
        newmob()
    # if pygame.sprite.collide_rect(player, enemySprite):
    #     running = False

    hits = pygame.sprite.spritecollide(player, mobs, False)
    if hits:
        for i in mobs:
            i.kill()
        player.rect.centerx = SCREEN_WIDTH / 2
        player.rect.bottom = SCREEN_HEIGHT - 10
        player.hide()
        # mobs.empty()
        # print(mobs)
        player.lives -= 1
        for i in range(8):
            newmob()
        # running = False
        slain = True

    if player.lives == 0:
        running = False

    screen.blit(background_image, background_position)

    all_sprites.draw(screen)
    draw_lives(screen, SCREEN_WIDTH - 100, 5, player.lives,
               player_mini_img)
    # screen.blit(playerSprite.image, playerSprite.rect)
    # screen.blit(enemySprite.image, enemySprite.rect)

    pygame.display.flip()
    clock.tick(30)

pygame.quit()

if slain:
    messagebox.showerror("Game over", "Mission Enabled")
