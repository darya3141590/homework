import pygame

#initialize game engine
pygame.init()

window_width=600
window_height=600

animation_increment=10
clock_tick_rate=20

#Open a window
size = (window_width, window_height)
screen = pygame.display.set_mode(size)

#Set title to the window
pygame.display.set_caption("Hello World")

dead=False

#Initialize values for color (RGB format)
WHITE=(255,255,255)
RED=(255,0,0)
GREEN=(40,114,51)
BLUE=(0,0,255)
BLACK=(0,0,0)
BROWN=(148,93,45)
PURPLE=(255,204,255)
#Initialize coords for circles
coords = [[270, 200], [285, 245], [330, 195], [210, 230], [348, 260], [305, 298],
              [355,322], [389, 310], [409, 282], [325, 370], [375, 360], [295, 425], [350, 410],
              [208, 298], [200, 375], [170, 330], [220, 430], [245,410]]
radius = [40, 15, 15, 15, 45, 15, 12, 12, 12, 33, 28, 15, 40, 45, 55, 30, 27, 11]
clickCoords = []

clock = pygame.time.Clock()
screen.fill(WHITE)

def draw_tree():
    pygame.draw.polygon(screen, BLACK, [[275, 299], [320, 269], [275, 321]])
    pygame.draw.polygon(screen, BROWN, [[275, 300], [320, 270], [275, 320]])
    pygame.draw.polygon(screen, BLACK, [[265, 471], [220, 429], [263, 449]])
    pygame.draw.polygon(screen, BROWN, [[265, 470], [220, 430], [263, 450]])
    pygame.draw.polygon(screen, BLACK, [[265, 334], [230, 309], [265, 361]])
    pygame.draw.polygon(screen, BROWN, [[265, 335], [230, 310], [265, 360]])
    pygame.draw.polygon(screen, BLACK, [[284, 434], [340, 414], [284, 456]])
    pygame.draw.polygon(screen, BROWN, [[284, 435], [340, 415], [284, 455]])
    pygame.draw.polygon(screen, BLACK, [[259, 541], [270, 199], [291, 541]])
    pygame.draw.polygon(screen, BROWN,[[260, 540], [270, 200],[290, 540]])
    for i in range(len(coords)):
        pygame.draw.circle(screen, BLACK, [coords[i][0], coords[i][1]], radius[i]+1)
        pygame.draw.circle(screen, GREEN, [coords[i][0], coords[i][1]], radius[i])
        for j in range(len(clickCoords)):
            if (coords[i] == clickCoords[j]):
                pygame.draw.circle(screen, PURPLE, [coords[i][0], coords[i][1]], radius[i])


while(dead==False):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            dead = True
    screen.fill((198, 230, 251))
    draw_tree()
    pygame.display.flip()
    clock.tick(clock_tick_rate)
    if (event.type == pygame.MOUSEBUTTONDOWN):
        x, y = event.pos
        for i in range(len(coords)):
            if ((coords[i][0] - x)**2 + (coords[i][1] - y)**2 <= radius[i]**2):
                if coords[i] in clickCoords:
                    clickCoords.remove(coords[i])
                    pygame.time.delay(100)
                else:
                    clickCoords.append(coords[i])
                    pygame.time.delay(100)

