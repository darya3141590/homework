def read_file(filename_read=""):
    with open(filename_read, 'r') as file:
        masivchik = []
        for s in file:
            c=s.split('\n')[0]
            c=c.split( )
            massiv=[]
            for k in c:
                k=int(k)
                massiv.append(k)
            masivchik.append(massiv)
    return masivchik

def rec(i, j, masivchik, mas):
    tekelement = masivchik[i][j]
    masivchik[i][j]=0
    mas.append(tekelement)
    try:
        if masivchik[i-1][j]==tekelement:
            if not masivchik[i-1][j]==masivchik[-1][j]:
                x = rec(i-1, j, masivchik, mas)
    except IndexError:
       x=0
    try:
        if masivchik[i][j + 1] == tekelement:
            if not masivchik[i][j+1]==masivchik[i][0]:
                x=rec(i, j+1, masivchik, mas)
    except IndexError:
        x=0
    try:
        if masivchik[i + 1][j] == tekelement:
            if not masivchik[i+1][j]==masivchik[0][j]:
                x = rec(i+1, j, masivchik, mas)
    except IndexError:
        x=0
    try:
        if masivchik[i][j - 1] == tekelement:
            if not masivchik[i][j-1]==masivchik[i][-1]:
                x = rec(i, j - 1, masivchik, mas)
    except IndexError:
        x=0
    return mas

masivchik=[]
filename_read = input("Введите имя файла с массивом: ")
masivchik=read_file(filename_read)
k=0
for i in range(len(masivchik)):
     for j in range(len(masivchik[i])):
         print(masivchik[i][j], end=' ')
     print()
for i in range(len(masivchik)):
     for j in range(len(masivchik[i])):
         if masivchik[i][j] == 0:
             continue
         else:
             mas=[]
             function=rec(i, j, masivchik, mas)
             k+=1
print("Количество пар-групп: ", k)