from math import sqrt
class Hexagon:
    def __init__(self, R):
        self.R=R
        self.calc_area()
    def set_R(self, new_R):
        self.R=new_R
        self.calc_area()
    def calc_area(self):
        self.area=(3*sqrt(3)*self.R**2)/2
    def __str__(self):
        return str(self.R)

h=Hexagon(2)
h.set_R(4)
print("Hexagon area: ", h.area)
print(h)