import copy
import time
from datetime import timedelta

def isPrime(n):
    if n==1:
        return False
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
        d += 2
    return d * d > n

def new_perestanovka(perestanovka, previous_perestanovka, inner):
    for i in range(-1, len(inner)-1):
        perestanovka[i+1]=previous_perestanovka[i]
    perestanovki.append(perestanovka)
    perestanovka=copy.deepcopy(perestanovki[-1])
    previous_perestanovka = copy.deepcopy(perestanovki[-1])
    return perestanovka, previous_perestanovka

start_time = time.monotonic()
chisla = []
k = 0
flag = False
suit = (1, 3, 7, 9)

for n in range(2, 9):
    if isPrime(n):
        chisla.append(n)
        
for n in range(10, 1000001):
    inner = []
    for i in range(len(str(n))):
        if int(str(n)[i]) not in suit:
            flag = False
            break
        else:
            inner.append(str(n)[i])
            flag = True
    if flag:
        perestanovka = copy.deepcopy(inner)
        previous_perestanovka = copy.deepcopy(inner)
        perestanovki = []
        perestanovki.append(inner)
        for i in range(len(inner)-1):
            perestanovka, previous_perestanovka = new_perestanovka(perestanovka, previous_perestanovka, inner)
            check = [isPrime(int(''.join(c))) for c in perestanovki]
            if False in check:
                break
            elif (i==len(inner)-2):
                chisla.append(n)
                
k = len(chisla)               
print(k, chisla)
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))