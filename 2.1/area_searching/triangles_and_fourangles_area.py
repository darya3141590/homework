import re
import copy
class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    def __repr__(self):
        return str((self.__x, self.__y))

    @property
    def get_x(self):
        return self.__x

    @property
    def get_y(self):
        return self.__y

    def __eq__(self, b):
        return self.__x == b.__x and self.__y == b.__y

    def __ne__(self, b):
        pass


class Figure:
    def __init__(self, points):
        self._pts = points
        self.__area = self._update_area()

    def _update_area(self):
        pass

    @property
    def area(self):
        return self.__area

    def __repr__(self):
        return str(self.__area)

    def __lt__(self, b):
        return self.__area < b.area

    def __le__(self, b):
        return self.__area <= b.area

    def __eq__(self, b):
        return self.__area == b.area

    def __ne__(self, b):
        return self.__area != b.area

    def __ge__(self, b):
        return self.__area >= b.area

    def __gt__(self, b):
        return self.__area > b.area


class Triangle(Figure):
    def _update_area(self):
        super()._update_area()
        return abs((self._pts[0].get_x - self._pts[2].get_x) * (self._pts[1].get_y - self._pts[2].get_y) - (self._pts[1].get_x - self._pts[2].get_x) * (self._pts[0].get_y - self._pts[2].get_y)) / 2


class Fourangle(Figure):

    def _update_area(self):
        super()._update_area()
        return abs((self._pts[0].get_x - self._pts[1].get_x)*(self._pts[0].get_y + self._pts[1].get_y) + (self._pts[1].get_x - self._pts[2].get_x) * (self._pts[1].get_y + self._pts[2].get_y) + (self._pts[2].get_x - self._pts[3].get_x) * (self._pts[2].get_y + self._pts[3].get_y) + (self._pts[3].get_x - self._pts[0].get_x) * (self._pts[3].get_y + self._pts[0].get_y))/2




ARRAY = ('[', ']', ',', ' ')
file = input('Введите имя файла: ')
with open(file, 'r') as f:
    for line in f:
        coordinates_preview = re.split("[|[|,| |]|]", line)
    for i in coordinates_preview:
        if i=="":
            coordinates_preview.pop(coordinates_preview.index(i))
    coordinates = ((int(coordinates_preview[i]), int(coordinates_preview[i+1])) for i in range(0, len(coordinates_preview), 2))
coordinates = set(coordinates)
coordinates = sorted(coordinates)

points = [Point(x, y) for x, y in coordinates]



# Создали множество площадей треугольников из набора точек
tr_area = {f"{points[i]}, {points[j]}, {points[k]}": Triangle((points[i], points[j], points[k])).area for i in range(len(
    points)-2) for j in range(i+1, len(points)-1) for k in range(j+1, len(points))}

def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k

tr_area_copy = copy.deepcopy(tr_area)
for key in tr_area_copy:
    if tr_area_copy[key]==0:
        del tr_area[key]
print(f'Максимальная площадь треугольника = {max(tr_area.values())}, вершинами являются {get_key(tr_area, max(tr_area.values()))}\nМинимальная = {min(tr_area.values())}, вершинами являются {get_key(tr_area, min(tr_area.values()))}')



# Создание множества площадей четырёхугольников из набора точек
four_area = []
used = []
def count(points_tuple):
    if (Fourangle((points[points_tuple[0]], points[points_tuple[1]], points[points_tuple[2]], points[points_tuple[3]])).area not in four_area) and (Fourangle((points[points_tuple[0]], points[points_tuple[1]], points[points_tuple[2]], points[points_tuple[3]])).area!=0):
        used.append((coordinates[points_tuple[0]], coordinates[points_tuple[1]], coordinates[points_tuple[2]], coordinates[points_tuple[3]]))
        four_area.append(Fourangle((points[points_tuple[0]], points[points_tuple[1]], points[points_tuple[2]], points[points_tuple[3]])).area)
    else:
        return

def pseudoinner_product(a, b, c):
    return float((b[0]-a[0])*(c[1]-a[1]) - (b[1]-a[1])*(c[0]-a[0]))

def convex(a, b, c, d):
    return (pseudoinner_product(a,b,c)*pseudoinner_product(a,b,d)<0 and pseudoinner_product(c,d,b)*pseudoinner_product(c,d,a)<0)

def check(i, j, k, l):
    variants = ((i, j, k, l), (i, j, l, k), (i, k, j, l))
    for c in variants:
        if convex(coordinates[c[0]],coordinates[c[2]],coordinates[c[1]],coordinates[c[3]]):
            count(c)
        else:
            continue


print("Подождите немного, программа старается выполнять работу активней, но иногда ей тоже лень...")
run = [check(i, j, k, l) for i in range(len(coordinates)-3) for j in range(i+1, len(coordinates)-2) for k in range(j+1, len(coordinates)-1) for l in range(k+1, len(coordinates))]
print(f'Точки у максимальной: {used[four_area.index(max(four_area))]}')
print(f'Точки у минимальной: {used[four_area.index(min(four_area))]}')
print(f'Максимальная площадь четырехугольника = {max(four_area)}, минимальная = {min(four_area)}.\nСпасибо за терпение!')
 

 
