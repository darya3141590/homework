class Points:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
    
    def __repr__(self):
        return str((self.__x, self.__y))
    
    @property
    def get_x(self):
        return self.__x

    @property
    def get_y(self):
        return self.__y
    
    

class Triangles:
    def __init__(self, a, b, c):
        self.__a = a
        self.__b = b
        self.__c = c
        self.__area = self.area_searching(self.__a, self.__b, self.__c)

    def area_searching(self, a, b, c):
        return abs((a.get_x - c.get_x) * (b.get_y - c.get_y) - (b.get_x - c.get_x) * (a.get_y - c.get_y)) / 2
    
    def __repr__(self):
        return str(self.area_searching(self.__a, self.__b, self.__c))

    def __gt__(self, second_triangle):
        return self.__area > second_triangle.__area

    def __lt__(self, second_triangle):
        return self.__area < second_triangle.__area
    
    def __eq__(self, second_triangle):
        return self.__area == second_triangle.__area

    def __ge__(self, second_triangle):
        return self.__area >= second_triangle.__area

    def __le__(self, second_triangle):
        return self.__area <= second_triangle.__area

    def __ne__(self, second_triangle):
        return self.__area != second_triangle.__area


ARRAY = ('[', ']', ',', ' ')
coordinates = []
one_point = ["", ""]
i = ""
k = 0
file = input('Введите имя файла: ')
with open(file, 'r') as f:
    for line in f:
        for s in line:
            if s not in ARRAY:
                i += s
            else:
                if i!="":
                    k += 1
                    if k%2 == 0:
                        one_point[1]=int(i)
                    else:
                        one_point[0] = int(i)
                    i=""
                    if (one_point[1]!="") and (one_point[0]!=""):
                        coordinates.append(one_point)
                        one_point=["", ""]

points_array = [Points(x, y) for x, y in coordinates]
min_area = Triangles(points_array[0], points_array[1], points_array[2])
max_area = Triangles(points_array[0], points_array[1], points_array[2])
for i in range(len(points_array)-2):
    for j in range(i+1, len(points_array)-1):
        for k in range(j+1, len(points_array)):
            current_area = Triangles(points_array[i], points_array[j], points_array[k]) 
            if current_area<min_area:
                min_area=current_area
                first_points = f"{points_array[i]}, {points_array[j]}, {points_array[k]}"
            elif current_area>max_area:
                max_area=current_area
                second_points = f"{points_array[i]}, {points_array[j]}, {points_array[k]}"
            
print(f'Максимальная площадь  = {max_area}, вершинами являются {second_points}')
print(f'Минимальная площадь  =  {min_area}, вершинами являются {first_points}')

print('Мини-проверка')
a = Triangles(Points(5, 6), Points(7, 1), Points(3, 2))
b = Triangles(Points(0, 1), Points(5, 9), Points(0, 0))

print('>:', a > b)
print('<:', a < b)
print('==:', a == b)
print('>=:', a >= b)
print('<=', a <= b)
print('!=:', a != b)

