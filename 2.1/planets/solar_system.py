import math

import pygame as pg

SCREEN_WIDTH, SCREEN_HEIGHT = 1280, 720

planets = {}

# в значение прописываю словарь, содержащий каждое свойсвто планеты как ключ и в значении - значение свойства
with open("properties_of_planets", 'r') as f:
    keys = f.readline().strip().split(' ')[1:]
    for i in f:
        planet = {}
        line = i.strip().split(' ')  # ?
        for key, ll in zip(keys, line[1:]):
            planet[key] = ll
        planets[line[0]] = planet


class Planet(pg.sprite.Sprite):
    def __init__(self, file_path, name, scale, your_angle, sun_alpha, R, r, coo_text, coo_planet):
        super().__init__()

        # ссылка на планету вокруг которой будет вращаться наша планета
        self.__major_planet = None

        # радиус вращения планеты вокруг major планеты
        self.__R = R * data_magnification_orbit
        self.__r = (r, r)
        # это коофицент размер планеты (ну типо размер)
        self.__scale = scale

        # это текущее изображение некое состояние (поворот планеты)
        self.__default_image = pg.image.load(file_path)
        # тут нормируем размер
        self.image = pg.transform.scale(self.__default_image, (moving_planet_magnification_factor * self.__scale, moving_planet_magnification_factor * self.__scale))
        # поле дефолтное состояние планеты, именно за счет него будет делать поворот
        self.__copy_image = self.image.copy()
        # это физическая модель, прямоугольник
        self.rect = self.image.get_rect()
        # здесь ставим центр нашей планеты на центр экрана
        self.rect.center = gameScreen.get_rect().center

        self.__name = name
        # центр планеты
        # self.center = self.rect.center

        # центр вращения вокруг major планеты, пока центр, птом выставим нужный
        self.__rotation_center = self.rect.center

        self.__your_angle = your_angle
        self.__sun_alpha = sun_alpha
        self.__remoteness = self.__sun_alpha

        self.__coo_text = coo_text
        self.__coo_planet = coo_planet
    # добавляем зависимость к major планетой
    def set_major_planet(self, planet):
        self.__major_planet = planet


    def draw(self):
        gameScreen.blit(pg.transform.scale(pg.transform.rotate(self.__default_image, draw_angle), self.__r),
                        self.__coo_planet)
        text = my_font.render(self.__name, True, (255, 255, 255))
        gameScreen.blit(text, self.__coo_text)

    # вращение вокруг планеты
    def __rotate_around_planet(self, angle):
        # вектор для того, что бы смещать нашу планету по траетрокии окружности
        offset = pg.Vector2(
            0, self.__R
        )
        # поворачиваем на угол
        offset = offset.rotate(angle)

        rotated_image = self.image
        rotated_rect = rotated_image.get_rect(center=self.__rotation_center + offset)
        return rotated_image, rotated_rect

    def __rotate_around_yourself(self, angle):
        # вращение вокруг себя, отличается тем, что нету offset
        rotated_image = pg.transform.rotate(self.__copy_image, -angle)
        rotated_rect = self.image.get_rect(center=self.rect.center)

        return rotated_image, rotated_rect

    # это главный метод обновления
    def update(self, tick):
        # тут вычисляем текущий угол вокруг себя, и вокруг планеты
        current_alpha, current_angle = -tick * self.__sun_alpha % full_circle, tick * self.__your_angle % full_circle
        self.__remoteness = current_alpha
        # тут обновляем центр, типо это точка цетнр major планеты
        self.__rotation_center = self.__major_planet.rect.center

        self.image, self.rect = self.__rotate_around_yourself(current_angle)
        self.image, self.rect = self.__rotate_around_planet(current_alpha)
        self.draw()
    
    @property
    def name(self):
        return self.__name
    @property
    def size(self):
        return f"Диаметр планеты: {self.__scale*data_magnification_factor*earth_diametr} км"
    @property
    def remotness_from_sun(self):
        return f"Удаленность от Солнца: {distance_factor_from_the_sun*self.__R} км"
    @property
    def planet_position(self):
        return f"Положение планеты в градусах: {self.__remoteness}"
    @property
    def orbit(self):
        return self.__R
    @property
    def draw_radius(self):
        return self.__r
    @property
    def coordinates_text_planet(self):
        return (self.__coo_text, self.__coo_planet)


pg.init()

gameScreen = pg.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pg.display.set_caption("Solar Sysytem")
background_position = [0, 0]
fon_position = [0, 530]
background_image = pg.image.load("images/fon6.jpeg")
fon = pg.image.load("images/stars.jpeg")
fon.set_alpha(150)
sound = pg.mixer.Sound("cosmos.mp3")
sound.play(-1)
sound.set_volume(0.2)
WHITE = (255, 255, 255)
draw_angle = 50
angle = 1
data_magnification_factor = 4
data_magnification_orbit = 10
moving_planet_magnification_factor = 50
distance_factor_from_the_sun = 1160000
earth_diametr = 12742
full_circle = 360

my_font = pg.font.SysFont("minionprocn", 15)

clock = pg.time.Clock()

pns = []
for planet in planets:
    name = planet
    discription = planets[planet]
    scale = float(discription['Radius(Earth=1)'])
    sun_alpha = float(discription['Rotation_Period_Sun'])
    your_angle = float(discription['Rotation_Period_yourself'])
    major_planet = discription['Major_Planet']
    R = float(discription['R'])
    path_to_file = f"./images/{discription['File_Name']}"
    r = int(discription['Drawn_size'])
    coo_text = (coor_text_x, coor_text_y) = int(discription['Text_coordinates_x']), int(discription['Text_coordinates_y'])
    coo_planet = (coor_planet_x, coor_planet_y)  = int(discription['Planet_coordinates_x']), int(discription['Planet_coordinates_y'])
    pn = Planet(path_to_file, name, scale, your_angle, sun_alpha, R, r, coo_text, coo_planet)
    pns.append(pn)

sprites_circle = pg.sprite.Group()

for planet in pns:
    if planet.name == 'Moon':
        planet.set_major_planet(pns[2])
    else:
        planet.set_major_planet(pns[-1])

    pg.draw.circle(gameScreen, (255, 255, 255), (gameScreen.get_rect().center[0], gameScreen.get_rect().center[1]), planet.orbit)
all_sprite = pg.sprite.Group()
all_sprite.add(pns)
is_running = True

while is_running:
    clock.tick(60)
    gameScreen.blit(background_image, background_position)
    gameScreen.blit(fon, fon_position)
    for event in pg.event.get():
        if event.type == pg.QUIT:
            is_running = False

    all_sprite.update(tick=angle)
    angle += 0.1
    pressed = pg.mouse.get_pressed()
    pos_down = pg.mouse.get_pos()
    for planet in pns:
        pg.draw.circle(gameScreen, (30, 30, 30), (gameScreen.get_rect().center[0], gameScreen.get_rect().center[1]), planet.orbit, 1)
    if pressed[0]:
        for planet in pns:
            if (pos_down[0] >= planet.coordinates_text_planet[0][0] - 5) and (pos_down[0] <= planet.coordinates_text_planet[1][0] + planet.draw_radius[0] - 5):
                if (pos_down[1] >= planet.coordinates_text_planet[0][1] - 15) and (pos_down[1] <= planet.coordinates_text_planet[1][1] + planet.draw_radius[0]):
                    pg.draw.rect(gameScreen, WHITE,
                                     pg.Rect(planet.coordinates_text_planet[1][0] + planet.draw_radius[0] - 55, planet.coordinates_text_planet[0][1] - 85, 220,
                                                 60))
                    text = my_font.render(planet.size, True, (0, 0, 0))
                    text2 = my_font.render(planet.remotness_from_sun, True, (0, 0, 0))
                    text3 = my_font.render(planet.planet_position, True, (0, 0, 0))
                    gameScreen.blit(text, (planet.coordinates_text_planet[1][0] + planet.draw_radius[0] - 50, planet.coordinates_text_planet[0][1] - 75))
                    gameScreen.blit(text2, (planet.coordinates_text_planet[1][0] +planet.draw_radius[0] - 50, planet.coordinates_text_planet[0][1] - 60))
                    gameScreen.blit(text3, (planet.coordinates_text_planet[1][0] +planet.draw_radius[0] - 50, planet.coordinates_text_planet[0][1] - 45))
    all_sprite.draw(gameScreen)
    sprites_circle.draw(gameScreen)
    pg.display.flip()
