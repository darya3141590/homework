import random
import time
from datetime import timedelta
import memory_profiler


@memory_profiler.profile
def task1(a, b, c):
    a, b, c = b, c, a
    return f"a = {a}, b = {b}, c = {c}"


def task21(a, b):
    try:
        float(a)
        float(b)
        return float(a) + float(b)
    except ValueError:
        a, b = input("Ошибка, введите еще раз два числа через запятую и пробел: ").split(', ')
        return task21(a, b)


def task22(n):
    try:
        array = [float(i) for i in input(f'Введите {n} чисел через запятую и пробел: ').split(', ')]
        return sum(array)
    except ValueError:
        print("Среди ваших чисел есть предатель")
        return task22(n)


@memory_profiler.profile
def task31(x):
    return x ** 5


@memory_profiler.profile
def task32(x, new_x):
    for i in range(4):
        new_x = x * new_x
    return f'Число {x} в 5 степени равно {new_x}'


def task4(a, fibonachchi):
    if (a >= 0) and (a <= 250):
        if a in fibonachchi:
            return "Число является числом фибоначчи"
        else:
            return "Число не является числом фибоначчи"
    else:
        return "Вы ввели неправильное число"


def task51(a):
    match a:
        case 1 | 2 | 12:
            return 'Зима'
        case 3 | 4 | 5:
            return 'Весна'
        case 6 | 7 | 8:
            return 'Лето'
        case 9 | 10 | 11:
            return 'Осень'
        case _:
            return 'Такого месяца нет'


def task52(a):
    dict = {"Весна": [3, 4, 5], "Лето": [6, 7, 8], "Осень": [9, 10, 11], "Зима": [12, 1, 2]}
    for k, v in dict.items():
        if a in v:
            return k


def task6(n, even, odd, summa):
    for i in range(1, n + 1):
        summa += i
        if i % 2 == 0:
            even += 1
        else:
            odd += 1
    return f"Количество четных равно {even}, количество нечетных равно {odd}, сумма равна {summa}"


@memory_profiler.profile
def task7(n):
    dict = {}
    k = 0
    if n < 250:
        for c in range(1, n + 1):
            for i in range(1, int(c / 2) + 1):
                if c % i == 0:
                    k += 1
            k += 1
            dict[c] = k
            k = 0
    return f'Числа до {n} и их количество делителей - {dict}'


def task8(n, m):
    piff = "Пиффагоровы тройки: "
    for i in range(n, m + 1):
        for j in range(n, m + 1):
            for k in range(n, m + 1):
                if i * i + j * j == k * k:
                    piff += str((i, j, k)) + " "
    return piff


def task9(n, m):
    chisla = "Числа, делящиеся на каждую из свои цифр: "
    for i in range(n, m + 1):
        k = True
        b = [i for i in map(int, str(i))]
        for c in range(len(b)):
            try:
                if i % b[c] == 0:
                    continue
                else:
                    k = False
            except ZeroDivisionError:
                k = False
        if k:
            chisla += str(i) + " "
    return chisla


def task10(n):
    if n < 5:
        chisla = f"Первые {n} совершенных чисел: "
        k = 0
        for i in range(1, 9999):
            divisors = []
            for c in range(1, int(i / 2) + 1):
                if i % c == 0:
                    divisors.append(c)
            if i == sum(divisors):
                chisla += str(i) + " "
                k += 1
                if k == n:
                    return chisla
    else:
        return "N не верное"


def task11(mas):
    return f"Первый способ {mas[-1]}, второй способ {mas[len(mas) - 1]}, третий способ {mas.pop(-1)}"


def task11_1(mas):
    return f"Первый способ {mas[-1]}"


def task11_2(mas):
    return f"Второй способ {mas[len(mas) - 1]}"


def task11_3(mas):
    return f"Третий способ {mas.pop(-1)}"


def task12(mas):
    mas.reverse()
    return mas


def task13(i):
    global summa
    if i == len(mas):
        return
    summa += mas[i]
    task13(i + 1)


def task15(n, m):
    if (n >= 5) and (m <= 20):
        for i in range(1, n + 1):
            for j in range(1, m + 1):
                print(i * j, end="\t")
            print('\n')
    else:
        print("Входные данные не соответствуют")


def task16():
    sea_battle = [["#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]]
    create_field = [sea_battle.append(["." for j in range(1, 11)]) for i in range(1, 11)]
    create_field = [sea_battle[i].insert(0, str(i)) for i in range(1, 11)]
    with open("ships.txt", "r") as f:
        for line in f:
            ships_list = line.split(", ")
            print(len(ships_list))
            for ships in ships_list:
                ships = ships.split("(")[1].split(")")[0]
                ships_data = ships.split(",")
                for i in range(1, len(sea_battle)):
                    if int(ships_data[1]) == i:
                        if ships_data[2] == "top":
                            for c in range(int(ships_data[-1])):
                                sea_battle[i - c][sea_battle[0].index(ships_data[0])] = "x"
                        elif ships_data[2] == "bottom":
                            for c in range(int(ships_data[-1])):
                                sea_battle[i + c][sea_battle[0].index(ships_data[0])] = "x"
                        elif ships_data[2] == "left":
                            for c in range(int(ships_data[-1])):
                                sea_battle[i][sea_battle[0].index(ships_data[0]) - c] = "x"
                        elif ships_data[2] == "right":
                            for c in range(int(ships_data[-1])):
                                sea_battle[i][sea_battle[0].index(ships_data[0]) + c] = "x"
                        else:
                            sea_battle[i][sea_battle[0].index(ships_data[0])] = "x"
    return sea_battle


print(
    "#Задача 1\nCоставить программу обмена значениями трех переменных а, b, c: так, чтобы b присвоилось значение c, с присвоить значение а, a присвоить значение b.\nСколько времени и памяти затратилось на выполнение программы?\nРешение:")
start_time = time.monotonic()
a, b, c = input("Введите три значения переменных через запятую и пробел: ").split(', ')
print(task1(a, b, c))
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

print(
    "\n#Задача 2.1\nКонсольная программа.\nПользователь вводит 2 числа. Проверьте, что это именно числа, если это не так, то выведите пользователю ошибку и попросите ввести число снова.\nКогда пользователь ввел числа, выведите сумму этих чисел.\nРешение:")
a, b = input("Введите два числа через запятую и пробел: ").split(', ')
print(task21(a, b))

print(
    "\n#Задача 2.2\nДоработайте задачу так, чтобы пользователь мог вводить n разных чисел. Предоставьте возможность ввести n самому пользователю.\nРешение:")
n = int(input("Введите n: "))
print(task22(n))

print("\n#Задача 3.1\nДано число x, которое принимает значение от 0 до 100. Вычислите чему будет равно x^5.\nРешение:")
start_time = time.monotonic()
x = random.randrange(0, 100)
new_x = x
print(task31(x))
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

print(
    "\n#Задача 3.2\nИзмените задачу так, чтобы для вычисления степени использовалось только умножение.\nПосмотрите сколько времени и памяти занимают оба метода. Что можно сделать для оптимизации данной задачи?\nРешение:")
start_time = time.monotonic()
print(task32(x, new_x))
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

print(
    "\n#Задача 4\nПользователь может вводить число от 0 до 250.\nПроверьте, принадлежит ли введенное число числам фибоначи\nРешение:")
fibonachchi = (0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233)
a = int(input("Введите число от 0 до 250: "))
print(task4(a, fibonachchi))

print(
    "\n#Задача 5\nРеализуйте программу двумя способами на определение времени года в зависимости от введенного месяца года.\nРешение:")
a = int(input('Введите цифру месяца: '))
print(task51(a))
print(task52(a))

print("\n#Задача 6\nПосчитайте сумму, количество четных и нечетных чисел от 1 до n. N вводит пользователь.\nРешение:")
n = int(input("Введите n: "))
even = 0
odd = 0
summa = 0
print(task6(n, even, odd, summa))

print(
    "\n#Задача 7\nДля каждого из чисел от 1 до n, где n меньше 250 выведите количество делителей. N вводит пользователь.\nВыведите число и через пробел количество его делителей. Делителем может быть 1.\nСколько памяти и времени занимает программа, что сделано для оптимизации затрачиваемых ресурсов?\nРешение:")
start_time = time.monotonic()
n = int(input("Введите n меньше 250: "))
print(task7(n))
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

print("\n#Задача 8\nНайти все различные пифагоровы тройки из интервала от N до М.\nРешение:")
n = int(input("Введите n: "))
m = int(input("Введите m: "))
print(task8(n, m))

print("\n#Задача 9\nНайти все целые числа из интервала от N до M, которые делятся на каждую из своих цифр.\nРешение:")
n = int(input("Введите n: "))
m = int(input("Введите m: "))
print(task9(n, m))

print(
    "\n#Задача 10\nНатуральное число называется совершенным, если оно равно сумме всех своих делителей, включая единицу. Вывести первые N (N<5) совершенных чисел на экран.\nРешение:")
n = int(input("Введите n: "))
print(task10(n))

print(
    "\n#Задача 11\nЗадайте одномерный массив в коде и выведите в консоль последний элемент данного массива тремя способами.\nСравните их по времени выполнения.\nРешение:")
mas = [1, 2, 3]
print(task11(mas))

start_time = time.monotonic()
task11_1(mas)
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

start_time = time.monotonic()
task11_2(mas)
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

start_time = time.monotonic()
task11_3(mas)
end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))

print("\n#Задача 12\nЗадайте одномерный массив в коде и выведите в консоль массив в обратном порядке\nРешение:")
mas = [1, 2, 3]
print(task12(mas))

print(
    "\n#Задача 13\nРеализуйте нахождение суммы элементов массива через рекурсию. Массив можно задать в коде.\nРешение:")
mas = [1, 2, 3, 5, 9]
summa = 0
task13(0)
print(summa)

print(
    "\n#Задача 15\nРеализуйте вывод таблицы умножения в консоль размером n на m которые вводит пользователь, но при этом они не могут быть больше 20 и меньше 5\nРешение:")
n = int(input("Введите n больше или равное 5: "))
m = int(input("Введите m меньше или равное 20: "))
task15(n, m)

print(
    "\n#Задача 16\nРеализуйте вывод в консоль поле для морского боя с выставленными кораблями.\nДанные о кораблях, можно подгружать из файла или генерировать самостоятельно\nРешение:")
sea_battle = task16()
for i in range(len(sea_battle)):
    for j in range(len(sea_battle[i])):
        print(sea_battle[i][j], end=' ')
    print()

print(
    "\n\#Задача 14.1\nРеализуйте оконное приложение-конвертер рублей в доллары.\nСоздайте окно ввода для суммы в рублях\n#Задача 14.2\nДоработайте приложение так, чтобы можно было переводить доллары в рубли.\nРешение:")
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import sys

root = Tk()
root.title("Конвертер валют")
buttons = [
    "1", "2", "3", "C", "Exit",
    "4", "5", "6", ".", "RUB",
    "7", "8", "9", "0", "USD"]
number_of_row = 1
number_of_col = 0
for i in buttons:
    command = lambda x=i: calculator(x)
    ttk.Button(root, text=i, command=command, width=7).grid(row=number_of_row, column=number_of_col)
    number_of_col += 1
    if number_of_col > 4:
        number_of_col = 0
        number_of_row += 1
entry = Entry(root, width=33)
entry.grid(row=0, column=0, columnspan=5)


def calculator(x):
    if (x == "USD") or (x == "RUB"):
        admissible = "0123456789."
        if entry.get()[0] not in admissible:
            messagebox.showerror("Error!", "Введенное число - не число")
        try:
            res = eval(entry.get())
        except:
            messagebox.showerror("Error!", "Проверьте правильность ввода")
    if x == "C":
        entry.delete(0, END)
    elif x == "RUB":
        x = float(entry.get())
        entry.delete(0, END)
        entry.insert(0, str(60.95 * x))
    elif x == "USD":
        x = float(entry.get())
        entry.delete(0, END)
        entry.insert(0, str(x / 60.95))
    elif x == "Exit":
        root.after(1, root.destroy)
        sys.exit
    else:
        if "=" in entry.get():
            entry.delete(0, END)
        entry.insert(END, x)


root.mainloop()
