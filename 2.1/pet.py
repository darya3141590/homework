from datetime import datetime as dt, timedelta
from threading import Timer
import sys
mood = ("Fuming", "Cross", "Haggard", "Low-spirited", "Normal", "High-spirited", "Contented", "Happy", "Excellent", "Perfect")
live = ("LIVE", "DEAD")
class Mood:
    Fuming = 1
    Cross = 2
    Haggard = 3
    Low_spirited = 4
    Normal = 5
    High_spirited = 6
    Contented = 7
    Happy = 8
    Excellent = 9
    Perfect = 10
class Live:
    LIVE = 1
    DEAD = 2
class Monster:
    def __init__(self, name, eat_timeout = 10):
        self.__name = name
        self.__eat_timeout = eat_timeout
        self.__st = dt.now()
        self.__mood = Mood.Fuming
        self.__state = Live.LIVE
    def eat(self, food, water, k):
        global yes_eat, yes_drink, answer, all_time, amount_w, amount_f
        self.__st = dt.now()
        if ((food == 0) or (water == 0)) and (all_time!=0):
            print("Мы забыли вам сказать, что нельзя доводить количество воды и еды до 0,\nиначе настроение питомца вернется на начальный уровень.")
            self.__mood = Mood.Fuming
            print("Настроение: ", mood[self.__mood - 1] + "\nСтатус жизни: ", live[self.__state - 1], "\n")
        else:
            print("Настроение: ", mood[self.__mood-1] + "\nСтатус жизни: ", live[self.__state - 1], "\n")
            if self.__mood == Mood.Perfect:
                print("Ты выиграл!!!")
                sys.exit()
        st_diff = (dt.now() - self.__st).seconds
        while st_diff < 32:
            st_diff = (dt.now() - self.__st).seconds
            if st_diff == 10:
                all_time += 10
                if (all_time % 60 == 0) and (all_time >= 60):
                    food += 1
                if (all_time % 40 == 0) and (all_time >= 40):
                    water += 1
                amount_f = food
                amount_w = water
                print(self.__name + ": " + "я хочу есть - корми")
                print("У вас осталось " + str(food) + " еды и " + str(water) + " воды")
                self.__st = dt.now()
                food, water, k = choose(food, water, k)
                if yes_eat:
                    food -= 1
                    if self.__mood == Mood.Excellent:
                        self.__mood = self.__mood + 1
                    else:
                        self.__mood = self.__mood + 2
                    self.__st = dt.now()
                    yes_eat = False
                    self.eat(food, water, k)
                elif yes_drink:
                    water -= 1
                    self.__mood = self.__mood + 1
                    self.__st = dt.now()
                    yes_drink = False
                    self.eat(food, water, k)
                else:
                    self.__st = dt.now() - timedelta(seconds=11)
                    if (answer == "поиграть") or (answer == "Поиграть"):
                        self.__mood = self.__mood + 1
                        print("Настроение: " + str(mood[int(self.__mood - 1)]) + "\n Статус жизни: " + str(
                            live[int(self.__state) - 1]))
                        continue
                    else:
                        if self.__mood == Mood.Fuming:
                            print("Настроение: " + str(mood[int(self.__mood)-1]) + "\n Статус жизни: " + str(live[int(self.__state) - 1]))
                            continue
                        elif self.__mood == Mood.Cross:
                            self.__mood = Mood.Fuming
                            print("Настроение: " + str(mood[int(self.__mood)-1]) + "\n Статус жизни: " + str(live[int(self.__state) - 1]))
                            continue
                        else:
                            self.__mood = self.__mood - 2
                            print("Настроение: " + str(mood[int(self.__mood-1)]) + "\n Статус жизни: " + str(live[int(self.__state) - 1]))
                            continue
            if st_diff == 20:
                all_time += 10
                if (all_time % 60 == 0) and (all_time >= 60):
                    food += 1
                if (all_time % 40 == 0) and (all_time >= 40):
                    water += 1
                amount_f = food
                amount_w = water
                print(self.__name + ": " + "я хочу есть - корми срочно")
                if (answer == "покормить") or (answer == "Покормить"):
                    food = 0
                print("У вас осталось " + str(food) + " еды и " + str(water) + " воды")
                food, water, k = choose(food, water, k)
                if yes_eat:
                    food -= 1
                    if self.__mood == Mood.Excellent:
                        self.__mood = self.__mood + 1
                    else:
                        self.__mood = self.__mood + 2
                    self.__st = dt.now()
                    yes_eat = False
                    self.eat(food, water, k)
                elif yes_drink:
                    water -= 1
                    self.__mood = self.__mood + 1
                    self.__st = dt.now()
                    yes_drink = False
                    self.eat(food, water, k)
                else:
                    self.__st = dt.now() - timedelta(seconds=21)
                    if (answer == "поиграть") or (answer == "Поиграть"):
                        self.__mood = self.__mood + 1
                        print("Настроение: " + str(mood[int(self.__mood - 1)]) + "\n Статус жизни: " + str(
                            live[int(self.__state) - 1]))
                        continue
                    else:
                        if self.__mood == Mood.Fuming:
                            print("Настроение: " + str(mood[int(self.__mood) - 1]) + "\n Статус жизни: " + str(
                                live[int(self.__state) - 1]))
                            continue
                        elif self.__mood == Mood.Cross:
                            self.__mood = Mood.Fuming
                            print("Настроение: " + str(mood[int(self.__mood) - 1]) + "\n Статус жизни: " + str(
                                live[int(self.__state) - 1]))
                            continue
                        else:
                            self.__mood = self.__mood - 2
                            print("Настроение: " + str(mood[int(self.__mood - 1)]) + "\n Статус жизни: " + str(
                                live[int(self.__state) - 1]))
                            continue
            if st_diff == 30:
                all_time += 10
                if (all_time % 60 == 0) and (all_time >= 60):
                    food += 1
                if (all_time % 40 == 0) and (all_time >= 40):
                    water += 1
                amount_f = food
                amount_w = water
                print(self.__name + ": " + "я хочу есть - я умираю")
                print("У вас осталось " + str(food) + " еды и " + str(water) + " воды")
                food, water, k = choose(food, water, k)
                if yes_eat:
                    food -= 1
                    if self.__mood == Mood.Excellent:
                        self.__mood = self.__mood + 1
                    else:
                        self.__mood = self.__mood + 2
                    self.__st = dt.now()
                    yes_eat = False
                    self.eat(food, water, k)
                elif yes_drink:
                    water -= 1
                    self.__mood = self.__mood + 1
                    self.__st = dt.now()
                    yes_drink = False
                    self.eat(food, water, k)
                else:
                    continue
            if st_diff > 30:
                self.__mood = Mood.Fuming
                self.__state = Live.DEAD
                print("Настроение: ", mood[self.__mood-1] + "\n Статус жизни: ", live[self.__state - 1])
                self.__state = Live.LIVE
                print(dead)
                k = True
                start(k)

k, yes_eat, yes_drink = False, False, False
all_time, amount_f, amount_w = 0, 0, 0
answer = ""
dead = "Ваш питомец умер, начните игру заново"
act = "Покормить, напоить, поиграть, убить или воскресить? НИЧЕГО НЕ ВВОДИТЬ! "
act_second = "На этом этапе вы можете воскресить питомца, либо окончательно убить: "
stop = "Воскресить не возможно, вы еще не умерли, продолжайте играть "
def time(food, water):
    global all_time, amount_w, amount_f
    all_time+=10
    if (all_time % 60 == 0) and (all_time >= 60):
        food += 1
    if (all_time % 40 == 0) and (all_time >= 40):
        water += 1
    amount_f = food
    amount_w = water
    return food, water

def choose(food, water, k):
    global yes_eat, yes_drink, answer
    if k != "точно умер":
        timeout = 10
        food, water = time(food, water)
        t = Timer(timeout, print, [ "\nВыбор окончен, введите ответ и нажмите Enter" + "\nУ вас осталось " + str(food) + " еды и " + str(water) + " воды"])
        t.start()
        prompt = act
        answer = input(prompt)
        t.cancel()
    else:
        prompt = act_second
        answer = input(prompt)
    if (answer == "") or ((answer == "поиграть") or (answer == "Поиграть")):
        pass
    elif (answer == "покормить") or (answer == "Покормить"):
        if food != 0:
            yes_eat = True
        else:
            food = 0
            print("еды нет")
    elif (answer == "напоить") or (answer == "Напоить"):
        if water != 0:
            yes_drink = True
        else:
            water = 0
            print("воды нет")
    elif (answer == "убить") or (answer == "Убить"):
        print(dead)
        if k == True:
            start(k)
        elif k == "точно умер":
            start(k)
        else:
            k = True
            start(k)
    elif (answer == "воскресить") or (answer == "Воскресить"):
        if not k:
            print(stop)
            choose(food, water, k)
        else:
            k = "воскрес"
            start(k)
    else:
        k = True
        print("Внимательно читайте варианты ответов, а мы с вами прощаемся")
        start(k)
    return food, water, k

def start(k):
    global amount_f, amount_w, all_time, m
    if (k == True) or (k == False) or (k == "точно умер"):
        if k == True:
            k = "точно умер"
            choose(amount_f, amount_w, k)
        else:
            k = False
            animal = input("Какое животное вы хотите завести? ")
            sex = input("Выберите пол (ж, м, ср): ")
            ur_name = input("Назовите питомца: ")
            congradulations = "Поздравляем, вы стали родителем!"
            print(congradulations)
            print("Каждые 10 секунд ваш питомец хочет есть, и после 30 секунд голодания, умирает.\nВы можете выбирать из списка действий любое, однако если вы будете с ним\nтолько играть, он вскоре погибнет.\nЧерез 10 секунд будет предложен выбор действий и на раздумие дается столько же времени.\nЦель игры: получить настроение 'Perfect'.\nЕсли кормите едой, то настроение повышается на 2 единицы, если водой - на 1 единицу.\nЕсли ничего не выбираете, тогда настроение падает на 2 единицы.\nЕдиница еды прибавляется каждые 60 секунд игры, воды - каждые 40 секунд.")
            print("Игра началась!")
            print("\n" + animal + " по имени " + ur_name + "," + " пол " + sex)
            amount_f = 0
            amount_w = 0
            all_time = 0
            m = Monster(ur_name)
            m.eat(amount_f, amount_w, k)
    elif k == "воскрес":
        m.eat(amount_f, amount_w, k)

start(k)