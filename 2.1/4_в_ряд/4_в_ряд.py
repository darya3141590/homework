import os
import sys
import pygame
import math

class Turn:
    color = {"красный": (199,37,26), "голубой": (22,104,181), "желтый": (236,216,50), "зеленый": (64,179,33)}

    def __init__(self, first_turn, sec_turn):
        self.__turn_dic = {first_turn: "1", sec_turn: "2"}
        self.__chip_color= {"1": self.color[first_turn], "2": self.color[sec_turn]}
        self.__state = self.__turn_dic[first_turn] #это 1

    def next_turn(self, first_turn, sec_turn):
        if self.__state == self.__turn_dic[first_turn]:
            self.__state = self.__turn_dic[sec_turn]
        else:
            self.__state = self.__turn_dic[first_turn]

    def current_turn(self):
        return self.__state
    
    @property
    def chip_color(self):
        return self.__chip_color

class Chip:

    __empty_cell = 0

    def __init__(self, state = __empty_cell):
        self.__state = state

    def is_empty(self):
        return self.__state == self.__empty_cell

    def set(self, state):
        if self.is_empty():
            self.__state = state
            return True
        else:
            return False

    def state(self):
        return self.__state

class Field:

    def __init__(self, COLUMN_COUNT, ROW_COUNT, SQUARESIZE):
        self.__width = COLUMN_COUNT*SQUARESIZE
        self.__height = ROW_COUNT*SQUARESIZE
        self.__true_size = (self.__width, self.__height)
        self.__screen = pygame.display.set_mode(self.__true_size)
        self.__fake_size = (COLUMN_COUNT, ROW_COUNT)
        self.__cells = []
        for i in range(0, COLUMN_COUNT):
            arr = []
            for j in range(0, ROW_COUNT):
                cell = Chip()
                arr.append(cell)
            self.__cells.append(arr)


    def draw(self, SQUARESIZE, RADIUS, color_array):
        for c in range(self.__fake_size[0]):
            for r in range(self.__fake_size[1]):
                pygame.draw.rect(self.__screen, BEIGE, (c*SQUARESIZE, r*SQUARESIZE+SQUARESIZE, SQUARESIZE, SQUARESIZE))
                pygame.draw.circle(self.__screen, WHITE, (int(c*SQUARESIZE+SQUARESIZE/2), int(r*SQUARESIZE+SQUARESIZE+SQUARESIZE/2)), RADIUS)
        
        for c in range(self.__fake_size[0]):
            for r in range(self.__fake_size[1]-1):		
                if self.__cells[r][c].state() == "1":
                    pygame.draw.circle(self.__screen, color_array["1"], (int(c*SQUARESIZE+SQUARESIZE/2), self.__height-int(r*SQUARESIZE+SQUARESIZE/2)), RADIUS)
                elif self.__cells[r][c].state() == "2": 
                    pygame.draw.circle(self.__screen, color_array["2"], (int(c*SQUARESIZE+SQUARESIZE/2), self.__height -int(r*SQUARESIZE+SQUARESIZE/2)), RADIUS)
        pygame.display.update()

    def set_cell(self, x, y, turn):
        return self.__cells[x][y].set(turn)

    def get_next_open_row(self, ROW_COUNT, col):
        for r in range(ROW_COUNT):
            if self.__cells[r][col].state() == 0:
                return r
    
    @property
    def field_size(self):
        return self.__true_size
    @property
    def screen(self):
        return self.__screen
    @property
    def cells(self):
        return self.__cells

class Game:

    def __init__(self, first_turn, sec_turn):
        self.__turn = Turn(first_turn, sec_turn) #кто ходит первый
        self.__next_turn = sec_turn
        self.__COLUMN_COUNT = int(input("Введите длину поля:"))
        self.__ROW_COUNT = int(input("Введите ширину поля:"))
        self.__COLUMN_COUNT =6
        self.__ROW_COUNT =6
        self.__SQUARESIZE = 100
        self.__RADIUS= int(self.__SQUARESIZE/2 - 5)
        self.__field = Field(self.__COLUMN_COUNT, self.__ROW_COUNT, self.__SQUARESIZE)
        self.run()

    def field_size(self, data):
        print(data)
        return int(input())
    
    def victory(self, piece):
        for c in range(self.__COLUMN_COUNT-3):
            for r in range(self.__ROW_COUNT):
                if self.__field.cells[r][c].state() == piece and self.__field.cells[r][c+1].state() == piece and self.__field.cells[r][c+2].state() == piece and self.__field.cells[r][c+3].state() == piece:
                    return True

        for c in range(self.__COLUMN_COUNT):
            for r in range(self.__ROW_COUNT-3):
                if self.__field.cells[r][c].state() == piece and self.__field.cells[r+1][c].state() == piece and self.__field.cells[r+2][c].state() == piece and self.__field.cells[r+3][c].state() == piece:
                    return True

        for c in range(self.__COLUMN_COUNT-3):
            for r in range(self.__ROW_COUNT-3):
                if self.__field.cells[r][c].state() == piece and self.__field.cells[r+1][c+1].state() == piece and self.__field.cells[r+2][c+2].state() == piece and self.__field.cells[r+3][c+3].state() == piece:
                    return True

        for c in range(self.__COLUMN_COUNT-3):
            for r in range(3, self.__ROW_COUNT):
                if self.__field.cells[r][c].state() == piece and self.__field.cells[r-1][c+1].state() == piece and self.__field.cells[r-2][c+2].state() == piece and self.__field.cells[r-3][c+3].state() == piece:
                    return True
        return False

    def run(self):
        while not self.victory(str((int(self.__turn.current_turn())+1)%2)):

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()

                if event.type == pygame.MOUSEMOTION:
                    pygame.draw.rect(self.__field.screen, WHITE, (0,0, self.__field.field_size[0], self.__SQUARESIZE))
                    posx = event.pos[0]
                    if self.__turn.current_turn() == "1":
                        pygame.draw.circle(self.__field.screen, self.__turn.chip_color["1"], (posx, int(self.__SQUARESIZE/2)), self.__RADIUS)
                    else: 
                        pygame.draw.circle(self.__field.screen, self.__turn.chip_color["2"], (posx, int(self.__SQUARESIZE/2)), self.__RADIUS)

                if event.type == pygame.MOUSEBUTTONDOWN:
                    posx = event.pos[0]
                    col = int(math.floor(posx/self.__SQUARESIZE))
                    self.turn(col)

                self.__field.draw(self.__SQUARESIZE, self.__RADIUS, self.__turn.chip_color)


        print("YOU WON!!!")
        
    def turn(self, col):
        row = self.__field.get_next_open_row(self.__ROW_COUNT, col)
        self.__field.set_cell(row, col, self.__turn.current_turn())
        self.__turn.next_turn(first_turn, self.__next_turn)
        


BEIGE = (224,211,193)
WHITE = (255,255,255)
COLOR = {"красный": (199,37,26), "голубой": (22,104,181), "желтый": (236,216,50), "зеленый": (64,179,33)}

def check(first, second):
    if first==second:
        print("Ошибка!")
        a = input("Первый игрок:")
        b = input("Второй игрок:")
        check(a, b)
        return a, b
    else:
        return first, second


pygame.init()
print("Выберите цвет вашей фишки (красный, желтый, зеленый, голубой")
first_turn = input("Первый игрок:")
sec_turn = input("Второй игрок:")
first_turn, sec_turn = check(first_turn, sec_turn)

myfont = pygame.font.SysFont("monospace", 75)
go= Game(first_turn, sec_turn)
