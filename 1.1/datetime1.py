import os
import datetime
from random import randint

dirname ='data'
os.mkdir(dirname)
for i in range(1, 365+1):
    current_ddd = '{:0>3}'.format(i)
    rand_hh = '{:0>2}'.format(randint(0,24))
    rand_mm = '{:0>2}'.format(randint(0,60))
    filename = f'./{dirname}/{current_ddd}_{rand_hh}_{rand_mm}.txt'
    with open(filename, 'w') as f:
        current_data = datetime.datetime.now()
        start_date = datetime.date(2021, 1, 1)
        end_date = datetime.date(current_data.year, current_data.month, current_data.day)
        time = abs((int((end_date - start_date).days+1)-int(current_ddd)))*86400
        time += abs((int(current_data.hour)-int(rand_hh)))*3600
        time += abs((int(current_data.minute)-int(rand_mm)))*60
        time += int(current_data.second)
        f.write(str(time))
