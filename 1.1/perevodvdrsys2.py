def perevodvdrsystem(a, b, d=""):
    while a>0:
        d+=chr(65+a%b-10) if a%b>9 else str(a%b)
        a//=b
    return d[::-1] if len(d)>0 else "0"
try:
    a=int(input("Введите число: "))
    b=int(input("(Введите цифру системы счисления: 2, 3, 8, 16): "))
    print(perevodvdrsystem(a, b, d=""))
except ValueError:
    print("Не то", end="!")

