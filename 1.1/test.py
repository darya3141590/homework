import tkinter as tk
from tkinter import *



def read_file():
     mas = []
     try:
         with open('text.txt', 'r') as file:
             for line in file:
                 mas.append((line.split('\n')[0]))
     except FileNotFoundError:
         print('File not found, check your filepath')

     return mas



# filename_read = input("Введите имя файла: ")
mas = read_file()
baseWindow=tk.Tk()
baseWindow.geometry("1000x2480")
s=0
p=0
counter = 0
def test():
    def que_one():
        que1 = tk.Label(baseWindow, text="1. " + mas[0], fg="#177245", wraplength=350, justify="center", font="Arial 18")
        que1.pack()
        r_var = tk.IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Audi", font="Arial 18",
                                      variable=r_var, justify="center", value=1)
        ans1.pack()
        ans2=tk.Radiobutton(baseWindow, fg="#F9423a", text="Volvo", font="Arial 18", variable=r_var, justify="center", value=2)
        ans2.pack()
        ans3=tk.Radiobutton(baseWindow, fg="#F9423a", text="BMV", font="Arial 18", variable=r_var, justify="center", value=3)
        ans3.pack()
        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 1:
                if result[0] == "3":
                    counter += 1
            que1.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            que_two()


        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                         bg="lightblue", borderwidth="5", width="35", justify="center", command= game1)
        plus.pack()

    def que_two():
        que2 = tk.Label(baseWindow, text="2. " + mas[1], fg="#177245", wraplength=350, justify="left", font="Arial 18")
        que2.pack()
        r_var = IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Bugatti Veyron", font="Arial 18",
                                      variable=r_var, justify="center", value=3)
        ans1.pack()
        ans2=tk.Radiobutton(baseWindow, fg="#F9423a", text="Bugatti EB110", font="Arial 18", variable=r_var, justify="center", value="1")
        ans2.pack()
        ans3=tk.Radiobutton(baseWindow, fg="#F9423a", text="Bugatti Type 35 GP", font="Arial 18", variable=r_var, justify="center", value=2)
        ans3.pack()
        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 2:
                if result[1] == "3":
                    counter += 1
                    print(counter)
            que2.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            que_three()
        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                         bg="lightblue", borderwidth="5", width="35", justify="center", command= game1)
        plus.pack()
    def que_three():
        que3 = tk.Label(baseWindow, text="3. " + mas[2], fg="#177245", wraplength=350, justify="left", font="Arial 18")
        que3.pack()
        r_var = IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Lamborghini Veneno", font="Arial 18",
                                      variable=r_var, justify="center", value=2)
        ans1.pack()
        ans2=tk.Radiobutton(baseWindow, fg="#F9423a", text="Lamborghini murcielago", font="Arial 18", variable=r_var, justify="center", value=3)
        ans2.pack()
        ans3=tk.Radiobutton(baseWindow, fg="#F9423a", text="Lamborghini 400 GT", font="Arial 18", variable=r_var, justify="center", value=1)
        ans3.pack()
        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 3:
                if result[2] == "3":
                    counter += 1
            que3.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            que_four()
        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                         bg="lightblue", borderwidth="5", width="35", justify="center", command= game1)
        plus.pack()
    def que_four():
        que4 = tk.Label(baseWindow, text="4. " + mas[3], fg="#177245", wraplength=350, justify="left", font="Arial 18")
        que4.pack()
        r_var = IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Дубай", font="Arial 18",
                                      variable=r_var, justify="center", value=3)
        ans1.pack()
        ans2=tk.Radiobutton(baseWindow, fg="#F9423a", text="ОАЗ", font="Arial 18", variable=r_var, justify="center", value=1)
        ans2.pack()
        ans3=tk.Radiobutton(baseWindow, fg="#F9423a", text="Египет", font="Arial 18", variable=r_var, justify="center", value=2)
        ans3.pack()
        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 4:
                if result[3] == "3":
                    counter += 1
            que4.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            que_five()
        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                         bg="lightblue", borderwidth="5", width="35", justify="center", command= game1)
        plus.pack()

    def que_five():
        que5 = tk.Label(baseWindow, text="5. " + mas[4], fg="#177245", wraplength=350, justify="left", font="Arial 18")
        que5.pack()
        r_var = IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Porsche 718", font="Arial 18",
                              variable=r_var, justify="center", value=1)
        ans1.pack()
        ans2 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Ferrari FXX", font="Arial 18", variable=r_var,
                              justify="center", value=3)
        ans2.pack()
        ans3 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Lamborghini Aventador", font="Arial 18", variable=r_var,
                              justify="center", value=2)
        ans3.pack()

        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 5:
                if result[4] == "3":
                    counter += 1
            que5.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            que_six()

        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                          bg="lightblue", borderwidth="5", width="35", justify="center", command=game1)
        plus.pack()
    def que_six():
        que6 = tk.Label(baseWindow, text="6. " + mas[5], fg="#177245", wraplength=350, justify="left", font="Arial 18")
        que6.pack()
        r_var = IntVar()
        ans1 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Aston Martin", font="Arial 18",
                              variable=r_var, justify="center", value=1)
        ans1.pack()
        ans2 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Porsche", font="Arial 18", variable=r_var,
                              justify="center", value=2)
        ans2.pack()
        ans3 = tk.Radiobutton(baseWindow, fg="#F9423a", text="Alpha Romeo", font="Arial 18", variable=r_var,
                              justify="center", value=3)
        ans3.pack()

        def game1():
            global counter
            global result
            result = str(result) + str(r_var.get())
            if len(result) == 6:
                if result[5] == "3":
                    counter += 1
            que6.pack_forget()
            ans1.pack_forget(), ans2.pack_forget(), ans3.pack_forget(), plus.pack_forget()
            augent=tk.Label(baseWindow, text="Итоговая сумма", fg="#177245", font="Arial 18")
            augent.pack()
            augent["text"] = "Итоговая сумма: " + str(counter)+"\n" + "Итоговый процент: " + str((counter * 100) / 6) + "%"

        plus = tk.Button(baseWindow, text="Ответить!", fg="#FF1678", font="Arial 18",
                          bg="lightblue", borderwidth="5", width="35", justify="center", command=game1)
        plus.pack()
    que_one()
result=""
test()







# print(s)
#     if r_var.get() == 'True':
#         tk.messagebox.showinfo('Congrats', message='You Are Correct')
#     else:
#         tk.messagebox.showinfo('Lose', message='You Are Wrong.')
#
# def on_click_button():
#     augent_text1['bg']='red'
#     augent_text1['fg'] = 'white'
#     augent_text2['bg']='red'
#     augent_text2['fg'] = 'white'
#     augent_text3['bg']='green'
#     augent_text3['fg'] = 'white'
#
# def read_file(filename_read=""):
#     mas = []
#     try:
#         with open(filename_read, 'r') as file:
#             for line in file:
#                 mas.append((line.split('\n')[0]))
#     except FileNotFoundError:
#         print('File not found, check your filepath')
#
#     return mas
#
#
# filename_read = input("Введите имя файла: ")
# mas = read_file(filename_read)
# root=Tk()
# baseWindow=tk.Tk()
# baseWindow.title(filename_read)
# baseWindow.geometry("1000x2480")
# scroll_x=tk.Scrollbar(baseWindow)
# scroll_x.pack(side="right", fill="y")
# scroll_x1=tk.Listbox(baseWindow, yscrollcommand=scroll_x.set)
# scroll_x.config(command=scroll_x1.yview())
# score=1
# augent=tk.Label(baseWindow, text= "1. " + mas[0], fg="#FF4678", wraplength=350, justify="left", font="Arial 18")
# augent.pack(side="top")
# r_var = StringVar()
# r_var.set("False1")
# augent_text1=tk.Radiobutton(baseWindow, fg="#FF1678", text="Audi", font="Arial 18", bg="lightblue", variable=r_var, justify="center", value="False1")
# augent_text1.pack()
# augent_text2=tk.Radiobutton(baseWindow, fg="#FF1678", text="Volvo", font="Arial 18", bg="lightblue", variable=r_var, justify="center", value="False2")
# augent_text2.pack()
# augent_text3=tk.Radiobutton(baseWindow, fg="#FF1678", text="BMV", font="Arial 18", bg="lightblue", variable=r_var, justify="center", value="True")
# augent_text3.pack()
# plus=tk.Button(baseWindow, command=gettingDecision, text="Ответить!", fg="#FF1678", font="Arial 18", bg="lightblue", borderwidth="5", width="35", justify="center")
# plus.pack()
# label=Label(width=20, height=10)
# label.pack()
#
#
# augent = tk.Label(baseWindow, text="2. " + mas[1], fg="#FF4678", wraplength=350, justify="left",
#                   font="Arial 18")
# augent.pack(side="top")
# augent_text3 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Bugatti Veyron", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="True")
# augent_text3.pack()
# augent_text2 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Bugatti EB110", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False1")
# augent_text2.pack()
# augent_text1 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Bugatti Type 35 GP", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False2")
# augent_text1.pack()
# plus = tk.Button(baseWindow, command=gettingDecision, text="Ответить!", fg="#FF1678", font="Arial 18",
#                  bg="lightblue", borderwidth="5", width="35", justify="center")
# plus.pack()
# label = Label(width=20, height=10)
# label.pack()
#
# augent = tk.Label(baseWindow, text="3. " + mas[2], fg="#FF4678", wraplength=350, justify="left",
#                   font="Arial 18")
# augent.pack(side="top")
# augent_text3 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Lamborghini murcielago", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="True")
# augent_text3.pack()
# augent_text2 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Lamborghini Veneno", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False1")
# augent_text2.pack()
# augent_text1 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Lamborghini 400 GT", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False2")
# augent_text1.pack()
# plus = tk.Button(baseWindow, command=gettingDecision, text="Ответить!", fg="#FF1678", font="Arial 18",
#                  bg="lightblue", borderwidth="5", width="35", justify="center")
# plus.pack()
# label = Label(width=20, height=10)
# label.pack()
#
# augent = tk.Label(baseWindow, text="4. " + mas[3], fg="#FF4678", wraplength=350, justify="left",
#                   font="Arial 18")
# augent.pack(side="top")
# augent_text3 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Дубай", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="True")
# augent_text3.pack()
# augent_text2 = tk.Radiobutton(baseWindow, fg="#FF1678", text="ОАЭ", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False1")
# augent_text2.pack()
# augent_text1 = tk.Radiobutton(baseWindow, fg="#FF1678", text="Египет", font="Arial 18", bg="lightblue",
#                               variable=r_var, justify="center", value="False2")
# augent_text1.pack()
# plus = tk.Button(baseWindow, command=gettingDecision, text="Ответить!", fg="#FF1678", font="Arial 18",
#                  bg="lightblue", borderwidth="5", width="35", justify="center")
# plus.pack()
# label = Label(width=20, height=10)
# label.pack()
# # plus = tk.Button(baseWindow, command=on_click_button, text="Ответить!", fg="#FF1678", font="Arial 18",
# #                  bg="lightblue", borderwidth="5", width="35", justify="center")
# # plus.pack()
baseWindow.mainloop()