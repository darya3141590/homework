from random import randint
def doubling(mas):
    return mas*2
mas=[randint(0,100) for i in range(0, 100)]
mas1=map(doubling, mas)
print("1.", *mas1)


def f(a): 
    return a[0]*a[1]*a[2] 
cc1 = [randint(0,100) for i in range(0, 100)]
cc2 = [randint(0,100) for i in range(0, 100)]
cc3 = [randint(0,100) for i in range(0, 100)]
cc4 = list(map(f, list(zip(cc1,cc2,cc3))))
print("2.", cc4)

def lena(a):
    return len(a)
cc1 = [str(randint(0,100)) for i in range(0, 100)]
print(cc1)
cc2 = list(map(lena,cc1))
print("3.", *cc2)


cc1 = [randint(0,100) for i in range(0, 100)]
cc2=list(filter(lambda x: x%2==0, cc1))
print("4.",*cc2)


cc1 = [randint(0,100) for i in range(0, 100)]
for s in range(len(cc1)):
    if cc1[s]=="0":
        cc1[s]=None
print(cc1)
cc2=list(filter(None, cc1))
print("5.",*cc2)

cc1 = [randint(0,100) for i in range(0, 100)]
cc2 = [randint(0,100) for i in range(0, 100)]
cc3 = [randint(0,100) for i in range(0, 100)]
cc4 = list(zip(cc1,cc2,cc3))
print("6.", *cc4, sep=";")


def f(a): 
    return a[0], a[1]*2 
cc1 = [randint(0,100) for i in range(0, 100)]
cc2 = [randint(0,100) for i in range(0, 100)]
cc4 = list(map(f, list(zip(cc1,cc2))))
print("7.", *cc4)