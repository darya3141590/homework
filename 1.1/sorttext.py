def insetion_sort(mas):
    buffer = 0
    i, j = 0, 0
    while(i < len(mas)):
        buffer = mas[i]
        j = i-1
        while(j >= 0 and buffer < mas[  j ]):
            mas[j+1] = mas[j]
            j-=1

        mas[j+1] = buffer
        i+=1
    return mas


def read_file(filename_read=""):
    mas = []
    try:
        with open(filename_read, 'r') as file:
            for line in file:
                mas.append(int(line.split('\n')[0]))
    except FileNotFoundError:
        print('File not found, check your filepath')

    return mas

def write_file(filename_write="", mas=[]):
    try:
        file = open(filename_write, 'x')
        for el in mas:
            file.write(str(el)+'\n')
    except FileExistsError:
        print('File already exist, overwrite it?')
        b = 0
        while 0 <= b <= 1:
            try:
                b = int(input("/t0-Yes/n/t1-No"))
            except ValueError:
                print("Retry")
            else:
                break
        if b == 0:
            file = open(filename_write, 'w')
            for el in mas:
                file.write(str(el) + '\n')


print("Работу выполнила Васильчук Дарья, группа 14122")
print("Данный код принимает на вход некоторый файл с неупорядоченными числами на каждой строчке, создает массив из строк, сортирует методом вставок и сохраняет изменения в выходной файл")
filename_read = input("Введите имя файла с числами: ")
filename_write = input("Введите имя выходного файла: ")

mas = read_file(filename_read)
mas = insetion_sort(mas)
write_file(filename_write, mas)

