flag = True
a, b = 0, 0
s = ''
while flag:
    try:
        a, b = map(int, input("Введите два числа = ").split())
    except ValueError:
        print("Вы ввели не число, попробуйте еще раз")
    else:
        flag = False

while s != '*' and s != '/' and s != '-' and s != '+':
    s=input("Введите действие (*/+-) = ")

try:
    if s=='/':
        s=a/b
except ZeroDivisionError:
    print("Нельзя", end="!")
else:
    if s=='+':
        s=a+b
    elif s=='-':
        s=a-b
    elif s=='*':
        s=a*b
    print(s)   

