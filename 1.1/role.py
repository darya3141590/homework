import os, sys


def is_role(t, names):
    for n in names:
        if n == t:
            return True
    return False


text3 = ''''''
try:
    with open('roles.txt', 'r', encoding='utf-8') as f:
        text3 = f.read()
        text3 = text3.split('\n')
except:
    print('Read problems')
    print('Please check your roles.txt file in same directory')
    sys.exit()

replic = {}
key = 0
words = []
for t in text3:
    if t == 'textLines:':
        replic['roles'] = words
        words = []
    elif t != 'roles:':
        words.append(t)
replic['text'] = words

key = ''
words = []
rep = {}
ch = 0
for te in replic['text']:
    ch += 1
    te = te.split(':', maxsplit=1)
    if is_role(te[0], replic['roles']):
        try:
            rep[key] = rep[key] + words
        except:
            rep[key] = words
        words = []
        key = te[0]
        words.append(str(ch) + ') ' + te[1])
    else:
        words.append(str(ch) + ') ' + te[0])

try:
    rep[key] = rep[key] + words
except:
    rep[key] = words

with open('answer.txt', 'w', encoding='utf-8') as f:
    for r in replic['roles']:
        f.write(r + ':' + '\n')
        try:
            for g in rep[r]:
                f.write("    " + g + '\n')
        except:
            f.write("    " + '[]' + '\n')
