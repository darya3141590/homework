# brackets_open - все открытые скобочки
# brackets_closed - все закрытые скобочки
# stack - массив, который содержит открытые скобки с вводимой строчки
# на случай если стэк пуст и идут закрытые скобки
# определяем индекс закрытой скобки и ищем по этому же индексу парную ей открытую скобку
# сравниваем крайнюю скобку в стэке и открытую скобку, если равны, откидываем крайнюю скобку и берем все до
def check(string):
    brackets_open = ('(', '[', '{', '<')
    brackets_closed = (')', ']', '}', '>')
    stack = []
    for i in string:
        if i in brackets_open:
            stack.append(i)
        if i in brackets_closed:
            if len(stack) == 0:
                return False
            index = brackets_closed.index(i)
            open_bracket = brackets_open[index]
            if stack[-1] == open_bracket:
                print(stack)
                stack = stack[:-1]
            else: return False
    return (not stack)
с=str(input("Введите выражение: "))
print(check(с))